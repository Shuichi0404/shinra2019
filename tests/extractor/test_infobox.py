from shinra2019.extractor import infobox as ibox
from shinra2019.util import util
from unittest import TestCase
import pkg_resources as pr
import logging

logging.basicConfig(filename='logfile/test.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestInfobox(TestCase):
    """
    Test case for Infobox class.
    """
    def test_parser(self):
        html = pr.resource_string("tests.extractor", "12766.html").decode()
        parser = ibox.InfoboxParser(logger=logger)
        parser.feed(html)
        parser.close()
        ptg = util.PartialTextGetter(html)
        for (k, v) in parser.data.items():
            print(f"key: '{k}'")
            print(f"pos: {v[0]}, {v[1]}")
            print(ptg.get_partial(v[0], v[1]))
            print()

    def test_parser2(self):
        html = pr.resource_string("tests.extractor", "12766.html").decode()
        parser = ibox.InfoboxParser(remove_tag=False, logger=logger)
        parser.feed(html)
        parser.close()
        ptg = util.PartialTextGetter(html)
        for (k, v) in parser.data.items():
            print(f"key: '{k}'")
            print(f"pos: {v[0]}, {v[1]}")
            print(ptg.get_partial(v[0], v[1]))
            print()
