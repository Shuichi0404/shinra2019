import unittest
from shinra2019.util import util
from shinra2019.err import ShinraError


class TestTagHistory(unittest.TestCase):
    def setUp(self):
        self.th = util.TagHistory()

    def test_oepn(self):
        self.th.clear()
        self.th.open("th")
        self.assertEqual(self.th.get()[0], "th")

    def test_close(self):
        self.th.clear()
        self.th.open("tr")
        self.th.close("tr")
        self.assertEqual(self.th.get(), [])
        self.th.open("tr")
        self.th.open("th")
        self.th.close("th")
        self.assertEqual(self.th.get(), ["tr"])
        self.th.open("th")
        self.th.open("p")
        self.assertEqual(self.th.get(), ["tr", "th", "p"])
        self.th.close("th")
        self.assertEqual(self.th.get(), ["tr"])

class TestPartialTextGetter(unittest.TestCase):
    def setUp(self):
        self.text = """sample
123
12345
"""
        self.tgt = util.PartialTextGetter(self.text)
        for (test, target) in zip([7,4,6], self.tgt._lines):
            self.assertEqual(test, len(target))

    def test_get_out_of_range(self):
        self.assertEqual(self.tgt.out_of_range((0,0)), False)
        self.assertEqual(self.tgt.out_of_range((0,6)), False)
        self.assertEqual(self.tgt.out_of_range((0,7)), False)
        self.assertEqual(self.tgt.out_of_range((0,8)), True)
        self.assertEqual(self.tgt.out_of_range((1,6)), True)
        self.assertEqual(self.tgt.out_of_range((3,6)), True)

    def test_get_partial(self):
        tests = [
            ((0,0), (0,7), "sample\n"),
            ((0,0), (2,6), self.text),
        ]
        for (st, ed, ans) in tests:
            try:
                self.assertEqual(self.tgt.get_partial(st, ed), ans)
            except ShinraError as e:
                print(st, ed, ans)
                raise e


class TestUtil(unittest.TestCase):
    def test_rip_off(self):
        text = \
"""<head><meta charset="utf-8"><title>TEST</title>
</head>
<body><p>Test</body>
"""
        ptg = util.PartialTextGetter(text)
        # list of (p1, p2, required)
        tests = [
            ((0, 35), (0, 39), ((0, 35), (0, 39))),
            ((0, 0), (0, 47), ((0, 35), (0, 39))),
            ((0, 0), (1, 7), ((0, 35), (0, 39))),
            ((2, 0), (3, 0), ((2, 9), (2, 13))),
        ]
        for (p1, p2, req) in tests:
            s = util.rip_off_tag(ptg, p1, p2)
            self.assertEqual(s, req)


if __name__ == "__main__":
    unittest.main()
