=========
Shinra 2019
=========

森羅プロジェクト2019 年版のために記述するスクリプト郡。
プロジェクト名やファイル名は今後変更され得る。
いまのところオフセットは全てゼロオリジンで作成されている。

とりあえずInfoboxのパーサを簡易ではあるが作成したので公開。

Infobox
=========
shinra2019.extractor.infobox.InfoboxParser がパーサの実体クラス。
このクラスの feedメソッドにhtmlを渡すことでInfoboxを取得可能。
なお、一度使ったあとはresetメソッドを呼ぶ必要があるかと考える。(未検証)

.. code-block:: python
   parser = shinra2019.extractor.infobox.InfoboxParser()
   parser.feed(HTML_AS_TEXT)
   parser.close()
   data = parser.data
   # Dict[str, Tuple[Tuple[int, int], Tuple[int, int]]]
   # Tuple[int, int] : (line, char)
   # (start, end)
   # str: infobox key
   infobox_range = parser.infobox_range
   # Tuple[Tuple[int, int], Tuple[int, int]]
   # (start, end)

キーワード引数として `logger` 及び `remove_tag` をとる．
logger は内部で使用する logging のインスタンスをとり，
remove_tag は両端のタグを外すかどうかを決定する．

Scripts
=========

メインとして統合したスクリプトを用意する?

extractor
---------

* htmlファイルやプレインテキストからなんらかの情報を抽出するためのPGはここ
* infoboxの抽出はここ
* 実際の抽出プログラムもここ？

Definitions
=========

Infobox
---------

* First sentence より後に存在する
* 最初のh2までに存在する
* table あるいは 定義済みリストで示される
* classに文字列 infobox を含む

以上を守るものがInfoboxと判断される。

Infobox 内容は非常に汚ない。テーブルで示される要素をそのまま持ってくれば…なんて甘い考えではいけない。

1. colspanが定義されている列は読み飛ばす
2. spanタグは読み飛ばす
3. aたぐは場合によって残る(鬼門)
4. ネストした表も探索する(この場合 1を無視することになるかも？)

基本的なInfoboxのためのクラスを記述しクラスによっては独自に拡張したクラスを用いてinfoboxを取得する？


Note
^^^^^^^^^
あまり目立たないがinfoboxとされるべきものの中には定義済みリストで記述されているものがある。
国名に頻出してる？

どうやら古いテンプレートを使っている場合定義済みリストになるらしい。
