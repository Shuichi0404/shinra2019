from setuptools import setup

setup(
    name="shinra2019",
    version="0.0.1",
    install_requires=["docopt"],
    extras_require={
        "develop": ["flake8", "autopep8", "bpython", "mypy", "isort"]
    },
)
