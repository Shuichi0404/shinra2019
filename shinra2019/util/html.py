def get_title(html: str):
    st = html.find("<title>") + len("<title>")
    ed = html.find("</title>")
    return html[st:ed]
