#!/usr/bin/env python

__doc__ = """ {f}
""".format(f=__file__)

import re
from shinra2019 import err
from typing import Tuple
import logging


logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class TagHistory:
    """
    Preserve a history of html tag.
    """
    def __init__(self):
        self._hist = []

    def open(self, tag: str):
        logger.debug(f"open tag: {tag}")
        self._hist.append(tag)

    def close(self, tag: str):
        logger.debug(f"close tag: {tag}")
        try:
            self._hist = self._hist[
                :-(list(reversed(self._hist)).index(tag)+1)
            ]
        except ValueError:
            pass

    def clear(self):
        logger.debug("refresh hist")
        self._hist.clear()

    def is_empty(self):
        return len(self._hist) == 0

    def get(self):
        return self._hist


def partial_text(text: str, l1, c1, l2, c2):
    """
    Return partial text in ``text`` between line `l1` char `c1`
    to line `l2` char `c2`

    """
    lst = text.split("\n")
    partials = []
    for (i, line) in enumerate(lst):
        if i < l1:
            continue
        elif l2 < i:
            break
        # 少なくても行としては範囲内
        elif i == l1:
            partials.append(line[c1:])
        elif i == l2:
            partials.append(line[:c2])
        else:
            partials.append(line)
    return "\n".join(partials)


class PartialTextGetter:
    def __init__(self, text):
        self.text = text
        self._lines = list(map(lambda x: x+"\n", text.split("\n")))

    def get_partial(self, p1: Tuple[int, int], p2: Tuple[int, int]):
        """
        Parameters
        ---------
        p1: Tuple[int, int]
           [line, char]
        p2: Tuple[int, int]
           [line, char]
        """
        if self.out_of_range(p1) or self.out_of_range(p2):
            of = ""
            if self.out_of_range(p1):
                of = "p1"
            if self.out_of_range(p2):
                of = "p2"
            raise err.ShinraError(f"Point out of range of {of}")
        partials = []
        for (i, line) in enumerate(self._lines):
            if i < p1[0]:
                continue
            elif p2[0] < i:
                break
            # 少なくても行としては範囲内
            elif i == p1[0] and p1[0] == p2[0]:
                partials.append(line[p1[1]:p2[1]])
            elif i == p1[0]:
                partials.append(line[p1[1]:])
            elif i == p2[0]:
                partials.append(line[:p2[1]])
            else:
                partials.append(line)
        return "".join(partials)

    def out_of_range(self, point: Tuple[int, int]):
        """
        """
        return point[0] > len(self._lines) or \
            len(self._lines[point[0]]) < point[1]

    def line_len(self, ln: int):
        """"""
        return len(self._lines[ln])


tag_seq_re = re.compile(r"([<>](\".*?\"|'.*?'|[^'\"])*?[<>]|[\n\s\t])*")


def rip_off_tag(ptg, p1, p2):
    """Rip off some tag(s) positioned in the most right(left) of text.

    Parameters
    ---------
    ptg: PartialTextGetter
    p1: Tuple[int, int]
    p2: Tuple[int, int]

    Returns
    ---------
    (r1, r2): Tuple[Tuple[int, int], Tuple[int, int]] updated positions
    """
    def _width(text, p, rev=False):
        logger.debug(f"ptg: {text}")
        logger.debug(f"ptg: {p}")
        if rev:
            p = (p[0], ptg.line_len(p[0]) - p[1])
        mt = tag_seq_re.match(text)
        if mt:
            upd = mt.end()
        else:
            upd = 0
        logger.debug(f"prg: upd: {upd}")
        ln = p[0]
        ch = p[1]
        while ch + upd > ptg.line_len(ln):
            upd -= ptg.line_len(ln) - ch
            logger.debug(f"prg: upd: {upd}")
            ln += -1 if rev else 1
            ch = 0
            logger.debug(f"prg: upd: ({ln}, {ch})")
        ch += upd
        return (ln, ch if not rev else ptg.line_len(ln) - ch)

    text = ptg.get_partial(p1, p2)
    # head
    head = _width(text, p1)
    logger.debug(f"prg: head: ({head[0]}, {head[1]})")
    # tail
    tail = _width("".join(reversed(text)), p2, rev=True)
    logger.debug(f"prg: tail: ({tail[0]}, {tail[1]})")
    return (head, tail)
