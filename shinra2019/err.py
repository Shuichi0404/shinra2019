class ShinraError(Exception):
    """Shinra Error
    Unexpected parameter or something wrong with this program.
    """
    pass
