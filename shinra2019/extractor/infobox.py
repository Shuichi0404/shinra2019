#!/usr/bin/env python

__doc__ = """ {f} get infobox with annotation from html file

Usage:
  {f} (-h|--help)
  {f} makeList <tgt_dir>
  {f} file [--bin <pickle>] <html_file>
  {f} list [--bin <pickle>] <list_file>
  {f} dir  [--bin <pickle>] <tgt_dir>

Options:
  <html_file>  		  html file to get infobox.
  <list_file>		  a list of html file.
  <tgt_dir> 		  a target directory. must end with "/" or "\\"
  --bin 			  print result and save result as pickle file.
  -h --help 		  show this help.

This program returns json includes infobox, text, html, title,...etc.
""".format(f=__file__)

from docopt import docopt
from html.parser import HTMLParser
from shinra2019.util import util, html as util_html
from shinra2019 import err
from logging.handlers import TimedRotatingFileHandler
import logging
import json
import glob
import pickle
import traceback
import os


def main(opt: dict):
    logging.basicConfig(level=logging.WARN)
    root_logger = logging.getLogger()
    handler = TimedRotatingFileHandler(
        filename="infobox.log",
        when="D",
        interval=1,
        backupCount=31,
    )
    formatter = logging.Formatter(
        '%(asctime)s %(name)s %(funcName)s [%(levelname)s]: %(message)s'
    )
    handler.setFormatter(formatter)
    root_logger.addHandler(handler)
    global _glogger
    _glogger = logging.getLogger(__name__)
    _glogger.addHandler(logging.NullHandler())
    parser = InfoboxParser(logger=_glogger, remove_tag=True)
    if opt["<html_file>"]:
        _glogger.info("single file mode")
        data = parse_single_file(opt["<html_file>"], parser=parser)
    elif opt["makeList"] or opt["dir"]:
        _glogger.info("making file list")
        files = glob.glob(opt["<tgt_dir>"] + "*.html")
        if opt["dir"]:
            data = multiple_files(files, parser=parser)
        else:
            for fn in files:
                print(fn)
            exit(0)
    elif opt["list"]:
        _glogger.info("multiple file mode")
        with open(opt["<list_file>"], "r") as f:
            data = multiple_files(f, parser=parser)
    else:
        _glogger.critical("unimplemented command")
        _glogger.critical(opt)
        _glogger.critical("will be exit.")
        exit(1)
    if opt["--bin"]:
        with open(opt["<pickle>"], "wb") as f:
            pickle.dump(data, f)
    print(json.dumps(data))


def multiple_files(file_itr, parser=None):
    if parser is None:
        parser = InfoboxParser(logger=_glogger, remove_tag=True)
    data = []
    for fn in file_itr:
        try:
            data.append(parse_single_file(fn))
        except ValueError:
            _glogger.error("going to next file.")
        parser.reset()
    return data


def parse_single_file(fn: str, parser=None):
    if parser is None:
        parser = InfoboxParser(logger=_glogger, remove_tag=True)
    with open(fn, "r") as f:
        text = f.read()
    title = util_html.get_title(text)
    bn = os.path.basename(fn)
    pid = bn[0:-len(".html")],
    _glogger.info(f"of {title} ({pid})")
    try:
        parser.feed(text)
        parser.close()
    except ValueError as e:
        _glogger.error(f"error in file: {fn}")
        _glogger.error(f"title: {title}, id: {pid}")
        _glogger.error(f"message: {e.args[0]}")
        _glogger.error(traceback.format_exc())
        raise e
    ptg = util.PartialTextGetter(text)
    data = {
        "title": title,
        "page_id": pid,
        "infobox": [{
            "key": k,
            "start": v[0],
            "end": v[1],
            "text": ptg.get_partial(v[0], v[1])
        } for (k, v) in parser.data.items()]
    }
    return data


class InfoboxParser(HTMLParser):
    """
    Implementation of Infobox parser.
    Find that seems to be infobox, and parse it.
    """
    _rst_tags = ["tr"]          # Reset Tags
    _hq_tags = ["th"]           # Header maybe tags
    _dt_tags = ["td"]           # Data maybe tags

    def __init__(self, *, logger=None, **keywords):
        self.logger = logger or _glogger
        super().__init__()
        if "remove_tag" in keywords and type(keywords["remove_tag"]) is bool:
            self.remove_tag = keywords["remove_tag"]
        else:
            self.remove_tag = True

    def feed(self, text):
        self._partial = util.PartialTextGetter(text)
        super().feed(text)

    def reset(self):
        super().reset()
        self._in_infobox = False
        self._stack = util.TagHistory()
        self._header = ""
        self._data_start = None
        self._header_start = None
        self._get_header = False
        self.data = dict()
        self.prt = False
        self._ib_stack = util.TagHistory()
        self._header_end = False
        self._data_end = False
        self._ib_end = False

    def handle_starttag(self, tag, attrs):
        """
        Be called when starting tag is found.

        Parameters
        ---------
        tag: str
            representation of tag as string.
        attrs: List[Tuple[str, str]]
            A dictionary of tag's attributes.
        """
        attrs = dict(attrs)     # Dict version of attrs
        self._check_end()
        if self._in_infobox:
            self._ib_stack.open(tag)
        if not self._in_infobox and \
           attrs.get("class") and \
           "infobox" in attrs.get("class"):
            # tag that has infobox as class is
            # seems to be infobox.
            self._in_infobox = True
            self._ib_stack.open(tag)
            self._ib_start = self.getpos()
        elif not self._in_infobox:
            # Not inside of the infobox
            # and not starting tag of infobox
            return
        elif tag in InfoboxParser._rst_tags:
            self._reset_stat()
        elif tag in InfoboxParser._hq_tags:
            # th tag. infobox key or to ignore entity.
            if attrs.get("colspan") and attrs["colspan"] == "2":
                # do not have data column so, ignore this line.
                return
            self._header_start = self.getpos()
            self._get_header = True
            self._stack.clear()
            self._stack.open(tag)
        elif tag in InfoboxParser._dt_tags and self._header != "":
            self._data_start = self.getpos()
            self._stack.open(tag)
        elif self._header_start:
            self._stack.open(tag)
        elif self._data_start:
            self._stack.open(tag)

    def handle_endtag(self, tag):
        """
        Be called when ending tag is found.

        Parameters
        ---------
        tag: str
            Representation of tag as string.
        """
        self._check_end()
        if self._in_infobox:
            self._ib_stack.close(tag)
            if self._ib_stack.is_empty():
                self._in_infobox = False
                self._ib_end = True
        if self._header_start and not self._header_end:
            self._stack.close(tag)
            if self._stack.is_empty() and tag in InfoboxParser._hq_tags:
                self._header_end = True
        elif self._data_start and not self._data_end:
            self._stack.close(tag)
            if self._stack.is_empty() and tag in InfoboxParser._dt_tags:
                self._data_end = True

    def handle_data(self, data):
        """
        Be called when inner of tag and not in the tag.

        Parameters
        ---------
        data: str
            String representation of data.
        """
        self._check_end()
        if self.prt:
            self.logger.debug(f"data: {data}")
            self.logger.debug(f"header?: {self._header_start}")
            self.prt = False

    def _reset_stat(self):
        self._header = ""
        self._data_start = None
        self._header_start = None
        self._get_header = False
        self._stack.clear()
        self._header_end = False
        self._data_end = False

    def getpos(self):
        (ln, cn) = super().getpos()
        return (ln-1, cn)

    def _check_end(self):
        if self._ib_end:
            self.logger.debug(f"IB END: {self.getpos()}")
            self.infobox_range = (self._ib_start, self.getpos())
            self._ib_end = False
        if self._header_end:
            self.logger.debug(
                f"header: {self._header_start}, {self.getpos()}"
            )
            if self.remove_tag:
                (s, e) = util.rip_off_tag(self._partial,
                                          self._header_start, self.getpos())
            else:
                (s, e) = (self._header_start, self.getpos())
            self.logger.debug(f"header: {s}, {e}")
            self._header = self._partial.get_partial(s, e)
            self.logger.debug(f"header: {self._header}")
            self._header_start = None
            self._stack.clear()
            self._header_end = False
        if self._data_end:
            self.logger.debug(f"data: {self._data_start}, {self.getpos()}")
            (s, e) = util.rip_off_tag(self._partial,
                                      self._data_start, self.getpos())
            self.data[self._header] = (s, e)
            self.logger.debug(f"data: {s}, {e}")
            self._data_start = None
            self._stack.clear()
            self._data_end = False


if __name__ == "__main__":
    main(docopt(__doc__))
